//index.js
//获取应用实例
const app = getApp()
const util = require("../../utils/backendcall.js");
Page({
  data: {
    motto: 'ServiceApp by IBsolution GmbH',
    history: [{ name: "test"}, {name: "test2"}],
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    loggedin : false,
    user: "S0020823277",
    pw: ""
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  }, 
  resetlogin: function (){
      console.log("reset");
      this.setData({
        loggedin : false
      });
  },
  handleTab: function(e){
    var that = this;
    console.log(this);
    
    this.data.history.forEach(function (element) {
      console.log(element);
      if(element.id == e.currentTarget.id){
        console.log("found " + e.currentTarget.id);
        that.showDetails(element.id);
      }
    });
  },
  login: function(e){
    this.setData({
      user : e.detail.value.user,
      pw : e.detail.value.pw
     });
    util.setAuth(this.data.user, this.data.pw);
    this.loadData();
  },
  loadData: function(){
    var target = this;
    console.log("loaddata"+this.data.user + this.data.pw );
    util.makeCall("request", null, (res) => {
      console.log(res);
      target.setData({
        history: res.data,
        loggedin: true
      })
    });
  },
  onLoad: function () {
    if(this.data.loggedin || util.isAuth())
    {
      this.loadData();
     
    }

  
    
  },
  openServicePage: function (){
    wx.navigateTo({
      url: '/pages/serviceRequest/serviceRequest',
    })
  },
  showDetails: function (request){
    wx.navigateTo({
      url: '/pages/serviceDetails/serviceDetails?id='+request,
    });
  },


  getUserInfo: function(e) {
    console.log(e);
    var target = this;
    wx.request({
      url: 'https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22',
     
      success: (res) => {
        console.log(res);
        target.setData({
          userInfo: res.data.error,
          hasUserInfo: true
        });
      }
    })
    // app.globalData.userInfo = e.detail.userInfo
    // this.setData({
    //   userInfo: e.detail.userInfo,
    //   hasUserInfo: true
    // })
  }
})
