// pages/serviceDetails/serviceDetails.js
const util = require("../../utils/backendcall.js");
Page({

  /**
   * Page initial data
   */
  data: {
    refid: '',
    request: { 
      id: "refid",
      machineid: "machineid",
      machinetype: "machinetype",
      servicedescription: "descr",
      contact: "contact",
      address: "address",
      phone: "phone"
    
    },
    loaded: false
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      refid : options.id
    });
    var target = this;
    util.makeCall("requestdetails", options.id, (res) => {
      console.log(res);
    
      target.setData({
        request: res.data[0],
        loaded:  res.data.length > 0 ? true : false
      })
    });
  },
  submitRequestDetail: function(e){
    console.log("refId"+ JSON.stringify(this));
    var rd = {
      machineid: e.detail.value.machineid,
      machinetype: e.detail.value.machinetype,
      servicedescription: e.detail.value.description, address: '', contact: '', phone: '',
      refid: e.detail.value.refid
    };
  
    console.log(JSON.stringify(rd));

    util.makePut("requestdetails", rd , () => {
      wx.navigateTo({
        url: '/pages/index/index',
      })
    });
  },
  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})