
var Base64 = require('base64.js').Base64;
var _user = null;
var _pw = null;


function make_base_auth(user, password) {
  console.log(user + "-"+password);
  var tok = user + ':' + password;
  var hash = Base64.encode(tok);
  return "Basic " + hash;
}
function setAuth(user,pw){
  this._user = user;
  this._pw = pw;
}
function isAuth(){
  if(this._user != null && this._pw != null) return true;
  return false;
}

function makeCall(target, crit, success)
{

  console.log("call: "+ target+ " crit "+crit);
  var params = target;
  if(crit != null) params += "?id=" +crit; 

   wx.request({
     url: 'https://hdda8aab308f.hana.ondemand.com/system-local/private/d574/helloIB/' + params,
     header: { Authorization: make_base_auth(this._user,this._pw)},
       success: (res) =>
       {
          success(res);
        

       }
     });
}

function makePut(target, payload, success){
  wx.request({
    url: 'https://hdda8aab308f.hana.ondemand.com/system-local/private/d574/helloIB/'+target,
    method: 'PUT',
    header: { Authorization: make_base_auth(this._user, this._pw) },
    data: JSON.stringify(payload),
    success: () => success()
    
  })
}

module.exports = {
  makeCall : makeCall,
  makePut: makePut,
  setAuth: setAuth,
  isAuth: isAuth
}